package game;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota[] pelotas;
	private Barra barra;
	private Image ima;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Pong - V1.0", 800, 600);

		// Inicializar lo que haga falta para el juego
		// ...
		this.ima = Herramientas.cargarImagen("Sky.png");
		this.barra = new Barra(400, entorno.alto() - 20, 200, 40);
		this.pelotas = new Pelota[3];
//		for (int i = 0; i < 3; i++) {
//			this.pelotas[i] = new Pelota(entorno.ancho() / 3 * (1 + i) - 50, entorno.alto() / 2);
//		}

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		entorno.dibujarImagen(ima, entorno.ancho() / 2, entorno.alto() / 2, 0);
//		entorno.dibujarCirculo(n, n, 50, Color.CYAN);
//		n++;
//		System.out.println(n);
		barra.dibujar(entorno);
		for (int i = 0; i < 3; i++) {
			if (pelotas[i] != null) {
				pelotas[i].mover();
				pelotas[i].dibujar(entorno);
				if (pelotas[i].llegaAlBorde(entorno)) {
					pelotas[i].rebotar(entorno);
				}

				if (pelotas[i].colisionaBarra(barra))
					pelotas[i].rebotaBarra(barra);

				if (entorno.sePresiono('s')) {
					pelotas[i].aumentarVelocidad();
				}
			}
		}
		if (entorno.estaPresionada(entorno.TECLA_DERECHA))
			barra.moverDerecha(entorno);
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA))
			barra.moverIzquierda();
		
		if(entorno.sePresiono('p')) {
			Pelota.agregarPelota(pelotas, barra.disparar());
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
