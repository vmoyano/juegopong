package game;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Pelota {
	// VARIABLES DE INSTANCIA o ATRIBUTOS
	private double x;
	private double y;
	private double diametro;
	private double angulo;
	private double velocidad;
	private Image ima;

//	CONTRUCTOR	
	public Pelota(double x, double y) {
		this.x = x;
		this.y = y;
		this.diametro = 50;
		this.angulo = -Math.PI/4;
		this.velocidad = 1+Math.random()*3;
		this.ima = Herramientas.cargarImagen("ball.png");

	}

//METODOS
	public void dibujar(Entorno e) {
		e.dibujarImagen(ima, x, y, 0, 0.15);
//		e.dibujarCirculo(x, y, diametro, Color.RED);
	}

	public void mover() {
		this.x = x + Math.cos(angulo)*velocidad;
		this.y = y + Math.sin(angulo)*velocidad;
	}

	public boolean llegaAlBorde(Entorno e) {
		if (x - diametro / 2 < 0) {
			return true;
		}
		if (x + diametro / 2 > e.ancho()) {
			return true;
		}
		if (y - diametro / 2 < 0) {
			return true;
		}
		if (y + diametro / 2 > e.alto()) {
			return true;
		}
		return false;
	}

	public void rebotar(Entorno e) {
		if (x - diametro / 2 < 0 || x + diametro / 2 > e.ancho()) {
			angulo = Math.PI - angulo;
		}
		if (y - diametro / 2 < 0 || y + diametro / 2 > e.alto()) {
			angulo = -1 * angulo;
		}
//		angulo = angulo+1;
	}

	public void aumentarVelocidad() {
		velocidad +=0.2;
		
	}
	
	public boolean colisionaBarra(Barra b) { 
	 boolean a = y + diametro/2 >= -b.alto/2 + b.y;	
	 boolean c = x + diametro/2 >= b.x - b.alto/2;
	 boolean d = x - diametro/2 <= b.x + b.ancho/2;
	 return a && c&& d;
}
	 
	public void rebotaBarra(Barra b) {
		if ((x + diametro / 2 - 1 < b.x - b.ancho / 2) || x - diametro / 2 + 1 > b.x + b.ancho / 2) {
			angulo = Math.PI - angulo;
		}
		if (colisionaBarra(b)) {
			angulo = -angulo;
		}
		while (colisionaBarra(b)) {
			mover();
		}
	}
	
	public static void agregarPelota(Pelota[] arr, Pelota p) {
//		arr[0] = p;
		for(int i = 0; i<arr.length; i++) {
			if(arr[i] == null) {
				arr[i] = p;
				return;
			}
		}
	}
	 
}
