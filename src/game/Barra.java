package game;

import java.awt.Color;

import entorno.Entorno;

public class Barra {
	double x;
	double y;
	double ancho;
	double alto;

	// constructor
	public Barra(double x, double y, double an, double al) {
		this.x = x;
		this.y = y;
		this.ancho = an;
		this.alto = al;
	}

	public void dibujar(Entorno e) {
		e.dibujarRectangulo(x, y, ancho, alto, 0, Color.BLACK);
	}

	public void moverDerecha(Entorno e) {
		if (x + ancho / 2 < e.ancho())
			this.x++;
	}

	public void moverIzquierda() {
		if (x - ancho / 2 > 0)
			this.x--;
	}
	
	public Pelota disparar() {
		return new Pelota(x, y-alto-50);
	}

}
